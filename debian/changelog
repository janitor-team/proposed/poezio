poezio (0.14-1) unstable; urgency=medium

  * New upstream version

 -- Martin <debacle@debian.org>  Tue, 12 Apr 2022 08:19:35 +0000

poezio (0.13.2-1) unstable; urgency=medium

  * New upstream version

 -- Martin <debacle@debian.org>  Sun, 06 Mar 2022 10:17:00 +0000

poezio (0.13.1-2) unstable; urgency=medium

  * Add patch to fix sphinx undefined URL_ROOT (Closes: #997446)
  * Set Section of python3-poezio-poopt to "python"
  * Bump dh compat level and standards version, no changes
  * Use HTTPS for homepage

 -- Martin <debacle@debian.org>  Sun, 24 Oct 2021 21:55:30 +0000

poezio (0.13.1-1) unstable; urgency=medium

  * New upstream version

 -- Martin <debacle@debian.org>  Wed, 10 Jun 2020 18:30:42 +0000

poezio (0.13-1) unstable; urgency=medium

  * New upstream version

 -- Martin <debacle@debian.org>  Mon, 25 May 2020 22:23:08 +0000

poezio (0.12.1-3) unstable; urgency=medium

  * add dependencies on python3-cffi and python3-pkg-resources
    (Closes: #917791, #921231)

 -- W. Martin Borgert <debacle@debian.org>  Sun, 03 Feb 2019 15:24:08 +0000

poezio (0.12.1-2) unstable; urgency=medium

  * use pybuild to drop tight dependency on Python 3.6 (Closes: #917628)

 -- W. Martin Borgert <debacle@debian.org>  Sat, 29 Dec 2018 23:07:38 +0000

poezio (0.12.1-1) unstable; urgency=medium

  * New upstream version

 -- W. Martin Borgert <debacle@debian.org>  Thu, 13 Sep 2018 22:05:36 +0000

poezio (0.11+git20180808-1) unstable; urgency=medium

  * New development snapshot

 -- W. Martin Borgert <debacle@debian.org>  Thu, 09 Aug 2018 20:51:01 +0000

poezio (0.11+git20180805-1) unstable; urgency=medium

  * New development snapshot
  * Add missing upstream names and years to debian/copyright
    (Closes: #905476)

 -- W. Martin Borgert <debacle@debian.org>  Mon, 06 Aug 2018 18:17:00 +0000

poezio (0.11+git20180331-1) unstable; urgency=medium

  * upload: now really (Closes: #592159)
  * remove obsolete patches
  * bump dh compat level and standards version

 -- W. Martin Borgert <debacle@debian.org>  Wed, 11 Apr 2018 23:34:07 +0000

poezio (0.10-2) unstable; urgency=medium

  * debian/control: explicitly depend on Python libraries that can be missed
    by dh_python3.
  * debian/patches:
     - use-packaged-gnupg.patch: make the gpg plugin import the gnupg library
       from the python3-gnupg package.
     - fix-gpg-plugin.patch: fix incorrect function calls in the gpg plugin.
  * debian/rules: remove the bundled gnupg library.
  * debian/copyright: add license of gnupg.py.

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Tue, 29 Nov 2016 21:11:30 +0100

poezio (0.10-1) unstable; urgency=medium

  * Initial release (Closes: #592159)

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Mon, 17 Oct 2016 13:55:06 +0200
